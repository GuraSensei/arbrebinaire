program arbrebinaire;

uses crt;
type t_data = integer;
type pnoeud = ^noeud;
	noeud = record
		data : t_data;
		gauche : pnoeud;
		droit : pnoeud;
	end;
var
	racine: pnoeud;
	n : integer;
	
PROCEDURE init(var racine : pnoeud; elt : t_data); //on remplit l'arbre 
BEGIN
	if (racine=nil) then
	BEGIN
		new(racine);
		racine^.data:=elt;
		racine^.gauche:=nil;
		racine^.droit:=nil;
	END
	else
	BEGIN
		if (racine^.gauche=nil) then 
		BEGIN
			init(racine^.gauche, elt);
		END
		else
		BEGIN
			init(racine^.droit, elt);
		END;
	END;
END;
	
PROCEDURE dfs(racine : pnoeud); //dfs en suffixer (selon la position du writeln en fait)
BEGIN
	if not(racine^.gauche=nil) then
		dfs(racine^.gauche);
	if not(racine^.droit=nil) then
		dfs(racine^.droit);
	writeln(racine^.data);
END;

BEGIN
	racine := nil;
	
	write('entrez un nombre (-1 pour arretez la saisie): ');
	readln(n);
	
	while(n >= 0) do
	BEGIN
		init(racine,n);
		write('entrez un nombre: ');
		readln(n);
	END;
	
	dfs(racine);
	writeln();
	
END.

